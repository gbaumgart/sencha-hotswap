import * as path from 'path';
import * as fs from 'fs';
import { read } from '../io/file';

const chokidar = require('chokidar');

export enum FileChangeType {
	'ADDED' = <any>'added',
	'CHANGED' = <any>'changed',
	'DELETED' = <any>'deleted',
	'INTERNAL' = <any>'internal'
}

export type FileChangedCallback = (path: string, type: FileChangeType, content?: string) => void;

export class FileWatcher {

	constructor(paths: string[], callback: FileChangedCallback) {
		this.init(paths, callback);
	}
	init(paths: string[], callback: FileChangedCallback) {
		const watcher = chokidar.watch(paths, {
			ignored: (path) => false,
			persistent: true,
			ignoreInitial: true,
			awaitWriteFinish: true
		});

		watcher.on('addDir', (filePath) => {
			watcher.add(filePath);
			callback(filePath, FileChangeType.INTERNAL);
		});
		watcher.on('add', (filePath) => callback(filePath, FileChangeType.ADDED));
		watcher.on('unlinkDir', (filePath) => {
			watcher.unwatch(filePath);
			callback(filePath, FileChangeType.DELETED);
		});
		watcher.on('change', (filePath) => callback(filePath, FileChangeType.CHANGED, read(filePath)));
	}
}
