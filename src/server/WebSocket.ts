import * as http from 'http';
import * as sockjs from 'sockjs';
import { isString } from '@xblox/core/primitives';
import { deserialize, serialize } from '../io/json';

export interface Options {
	port: number;
	host: string;
	debug: number;
}
export class WebSocket {

	socket: any;
	socketServer: any;
	options: Options;
	clients: Array<any>;

	_handler() { }

	constructor(options: Options) {
		this.options = options;
		this.clients = [];
		const socketOptions = options;
		const socket = sockjs.createServer(socketOptions);
		const socket_server = http.createServer(this._handler);
		const port = socketOptions.port;
		const host = socketOptions.host;
		socket.installHandlers(socket_server);
		socket_server.listen(port, host);
		this.handleSocketEmits(socket);
		this.socket = socket;
		this.socketServer = socket_server;
	}
	broadCastMessage(eventName: string, data: string | Object) {
		let length = this.clients.length;
		while (length--) {
			const client = this.clients[length];
			if (client !== undefined) {
				const dataOut = {
					event: eventName,
					data: isString(data) ? deserialize(data) : data
				};
				if (client.readyState === 3) {//Sockjs bug, connection not closed
					client.close();
					this.clients.splice(length, 1);
					continue;
				}
				this.clients[length].write(serialize(dataOut));
			}
		}
	}

	handleSocketEmits(socket) {
		const self = this;
		socket.on('connection', (conn) => {
			this.clients.push(conn);
			conn.on('close', function () {
				self.clients.splice(self.clients.indexOf(conn), 1);
				console.log('closed', self.clients.length);
			});
		});
	}
}
