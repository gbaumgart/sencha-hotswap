#!/usr/bin/env node
import * as yargs_parser from 'yargs-parser';
import * as pathUtil from 'path';
import * as cli from 'yargs';
import { existsSync } from 'fs';
import { WatcherOptions } from '../applications/sencha-hotswap/index';
import { createWithCLI, normalizePathString } from '../index';
import { coalesce } from '@xblox/core/arrays';

const _argv = yargs_parser(process.argv.slice(1));
const isDev = _argv['_'][0].indexOf('.ts') !== -1;

const normalizePathArray = (paths: string[]): string[] => {
	return coalesce<string>(paths.map((path) => {
		try {
			if (!existsSync(pathUtil.resolve(path))) {
				console.error('Cant resolve path ' + path);
				return null;
			}
		} catch (e) { }

		return pathUtil.resolve(path);
	}));
}

const defaultArgs = (yargs) => {
	return yargs.option('target', {
		alias: 'target',
		"default": process.cwd()
	});
}
if (isDev) {
	console.error('bin is in dev');
	process.exit();
}


cli.options('v', {
	alias: 'version',
	description: 'Display version number'
});

cli.options('w', {
	alias: 'watch',
	description: 'Comma separated list of paths to watch, if not specified it will look in ' +
	'in the current directories`s package.json for {"hotswap":{ "paths" : .. '
});

const argv = cli.argv;
const cwd = process.cwd();

if (argv.h || argv.help) {
	cli.showHelp();
	process.exit();
} else if (argv.v || argv.version) {
	const pkginfo = require(pathUtil.join(cwd, './package.json'));
	console.log(pkginfo.version);
	process.exit();
}

if (argv.watch) {
	createWithCLI({
		paths: normalizePathString(argv.watch),
		port: argv.port || 19999
	});
} else {
	try {
		if (existsSync(pathUtil.join(cwd, './package.json'))) {
		} else {
			console.error('No --watch argument specified and current directory doesnt contain a package.json, abort!');
			process.exit();
		}
	} catch (e) {

	}
	const pkginfo = require(pathUtil.join(cwd, './package.json'));
	if (!pkginfo.hotswap) {
		console.error('package.json doesnt contain any hotswap key, abort!');
		process.exit();
	}

	if (!pkginfo.hotswap.watch) {
		console.error('The hotswap object in package.json doesnt contain any watch key, abort!');
		process.exit();
	}



	const paths = normalizePathArray(pkginfo.hotswap.watch);
	if (!paths.length) {
		console.error('Have no paths to watch, abort');
		process.exit();
	}

	createWithCLI({
		paths: paths,
		port: argv.port || 19999
	});
}
