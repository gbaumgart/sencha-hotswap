import * as _ from 'lodash';
import * as path from 'path';
import { IApplication, Options, ELayout, EEKey } from './../Base';
import { IObjectLiteral } from '../../interfaces/index';
import { List } from '../../interfaces/index';
import { BaseService } from '../../services/Base';
import * as yargs from 'yargs-parser';
import { WebSocket, Options as WebSocketOptions } from '../../server/WebSocket';
import { FileWatcher, FileChangeType, FileChangedCallback } from '../../server/FileWatcher';
const esprima = require('esprima');
const esquery = require('esquery');

const DEBUG = false;

const SCRIPTS = /\.(js)$/i;
const STYLES = /\.(css)$/i;

const EXTEND = 'CallExpression > ObjectExpression > Property[key.name="extend"] [value]';

export type WatcherOptions = Options & {
	paths: string[];
}
export interface ExtUpdateMessage {
	path: string;
	class?: string;
	extends?: string;
}

export class Watcher {
	options: WatcherOptions;
	socket: WebSocket;
	watcher: FileWatcher;
	constructor(options: WatcherOptions) {
		this.options = options;
	}

	onChanged(path: string, type: FileChangeType, content: string) {
		switch (type) {
			case FileChangeType.CHANGED: {
				if (path.match(STYLES)) {
					DEBUG && console.log('changed ' + path + ' Type : ' + type);
				}
				if (path.match(SCRIPTS)) {
					try {
						const ast = esprima.parse(content);
						const clazz = ast.body[0]['expression']['arguments'][0].value;
						const extend = esquery.query(ast, EXTEND);
						this.socket.broadCastMessage('FileChanged', {
							path: path,
							class: clazz,
							extent: extend,
							content: content
						});
						console.log('changed class ' + clazz + ' @ ' + path);
					} catch (e) {
						console.error('Error parsing script ' + path);
					}
				}
			}
		}
	}
	run(): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			this.socket = new WebSocket({
				"port": this.options.port,
				"host": "0.0.0.0",
				"debug": 0
			});
			this.watcher = new FileWatcher(this.options.paths, (path: string, type: FileChangeType, content: string) => {
				this.onChanged(path, type, content);
			});
		});
	}
}
