import { IObjectLiteral, List } from '../interfaces/index';
import { EPersistence } from '../interfaces/Application';
import { BaseService } from '../services/Base';

import * as _ from 'lodash';
const http = require('http');
const SRC_DIR = '/Code/client/src/';
const LIB_DIR = SRC_DIR + 'lib/';
const destroyable = require('server-destroy');
const io = {
	serialize: JSON.stringify
};
export const ENV = {
	VARIABLES: {
		SRC_DIR: SRC_DIR,
		LIB_DIR: LIB_DIR,
		'XAS_WEB': function (origin) {
			return origin + LIB_DIR;
		},
		'APP_URL': function (origin) {
			return origin + SRC_DIR;
		},
		'RPC_URL': function (origin) {
			return origin + '/smd';
		},
		APP_ROOT: function (app: ApplicationBase) {
			return app.config['APP_ROOT'];
		},
		CLIENT_ROOT: function (app: ApplicationBase) {
			return app.config['CLIENT_ROOT'];
		},
		RELEASE: function (app: ApplicationBase) {
			return app.config['RELEASE'];
		}
	}
};
export const EEKey = {
	'XAS_WEB': 'XAS_WEB',
	'APP_URL': 'APP_URL',
	'RPC_URL': 'RPC_URL',
	'RESOURCE_VARIABLES': 'RESOURCE_VARIABLES',
	'DOJOPACKAGES': 'DOJOPACKAGES',
	'THEME': 'THEME',
	'APP_ROOT': 'APP_ROOT',
	'DATA_ROOT': 'DATA_ROOT',
	'DB_ROOT': 'DB_ROOT',
	'BASE_URL': 'BASE_URL',
	'CLIENT_ROOT': 'CLIENT_ROOT',
	'LIB_DIR': ENV.VARIABLES.LIB_DIR,
	'RELEASE': 'RELEASE',
	'NODE_ROOT': 'NODE_ROOT',
	'ROOT': 'ROOT',
	'VFS_URL': 'VFS_URL',
	'VFS_CONFIG': 'VFS_CONFIG',
	'SYSTEM_ROOT': 'SYSTEM_ROOT',
	'USER_DIRECTORY': 'USER_DIRECTORY',
	'DEVICES': 'devices',
	'DRIVERS': 'drivers',
	'WORKSPACE': 'workspace',
	'USER_DEVICES': 'user_devices',
	'USER_DRIVERS': 'user_drivers',
	'SYSTEM_DRIVERS': 'system_drivers',
	'SYSTEM_DEVICES': 'SYSTEM_DEVICES'
};

export enum ELayout {
	'NODE_JS' = <any>'NODE_JS',
	'SOURCE' = <any>'SOURCE',
	'OFFLINE_RELEASE' = <any>'OFFLINE_RELEASE'
};

export const ESKey = {
	'SettingsService': 'settingsService'
};

export function ENV_VAR(key: string) {
	return ENV.VARIABLES[key];
}
export interface IApplication {
	rpcServices(): List<BaseService>;
	setup(): void;
	run(): void;
	config: IObjectLiteral;
	uuid: string;
}
export interface Options {
	port?: number;
	root?: string;
	release?: boolean;
	clientRoot?: string;
	type?: ELayout;
	nodeRoot?: string;
	print?: boolean;
	uuid?: string;
	persistence?: EPersistence;
}
type StringOrFunction = string & Function;

export class ApplicationBase implements IApplication {
	readonly config: IObjectLiteral;
	uuid: string = 'no uuid';
	server: any;

	setup() { }

	public path(key: string): string {
		return this.config[key];
	}

	public relativeVariable(key: string, value?: string) {
		if (value != null) {
			this.config['relativeVariables'][key] = value;
		}
		return this.config['relativeVariables'][key];
	}

	public _env(origin: string, key: string): StringOrFunction {
		switch (key) {
			case EEKey.APP_ROOT: {
				return this.config[EEKey.APP_ROOT];
			}
			case EEKey.CLIENT_ROOT: {
				return this.config[EEKey.CLIENT_ROOT];
			}
		}
		return ENV.VARIABLES[key];
	}


	rpcServices(): List<BaseService> {
		return [];
	}

	async boot(): Promise<any> {
		return Promise.resolve(null);
	}

	run(): void {
		/*
		const koaHandler = this.callback();
		this.server = http.createServer(koaHandler);
		this.use(async (ctx, next) => {
			ctx.req.socket.setNoDelay(true);
			await next();
		});
		destroyable(this.server);
		*/
	}
	packages(offset: string, baseUrl?: string): List<IObjectLiteral> {
		return [];
	}
}
