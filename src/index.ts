import { Options, ELayout } from './applications/Base';
import { Watcher, WatcherOptions } from './applications/sencha-hotswap/index';
import { EPersistence } from './interfaces/Application';
import * as pathUtil from 'path';
import { existsSync } from 'fs';
import * as yargs_parser from 'yargs-parser';
import { coalesce } from '@xblox/core/arrays';
import { inspect } from 'util';

//just to include it in the build
const app = "sencha-hotswap";

process.on('unhandledRejection', (reason) => {
	console.error('Unhandled rejection, reason: ', reason);
});

const _argv = yargs_parser(process.argv.slice(1));
let isCli = _argv['_'][0].indexOf('sencha-hotswap') !== -1;
if(_argv.dev){
	isCli = false;
}
export const normalizePathString = (str: string): string[] => {
	return coalesce<string>(str.split(',').map((path) => {
		try {
			if (!existsSync(pathUtil.resolve(path))) {
				console.error('Cant resolve path ' + path);
				return null;
			}
		} catch (e) { }

		return pathUtil.resolve(path);
	}));
}

if (!isCli) {
	const argv = yargs_parser(process.argv.slice(2));
	if (!argv.watch && !isCli) {
		console.error('You need to specicfy paths to be watched with --watch=...');
		process.exit(1);
	}

	const options: WatcherOptions = {
		paths: normalizePathString(argv.watch),
		port: argv.port || 19999
	};

	if (!options.paths.length) {
		console.error('Have no paths to watch, abort');
		process.exit(1);
	}

	console.log('Watching paths : ', inspect(options.paths));
	const application = new Watcher(options);
	application.run().then((deviceServerContext) => { });
}

export function createWithCLI(options: WatcherOptions) {
	console.log('Watching paths : ', inspect(options.paths));
	const application = new Watcher(options);
	application.run().then((deviceServerContext) => { });
}

