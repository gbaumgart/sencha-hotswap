import { IObjectLiteral } from '../interfaces/index';
import { IStoreAccess, IStoreIO } from '../interfaces/Store';
import { BaseService, RpcMethod } from '../services/Base';
import * as dotProp from 'dot-prop';
import * as _ from 'lodash';
import * as pathUtil from 'path';

/**
 * This service sets/gets data in a json file, utilizing 'dot-prop' to select certain data in the object.
 *
 * @export
 * @class JSONFileService
 * @extends {BaseService}
 * @implements {IStoreIO}
 * @implements {IStoreAccess}
 */
export class JSONFileService extends BaseService implements IStoreIO, IStoreAccess {
	public method: string = 'XApp_Store';
	public root: string;
	constructor(config: string) {
		super(config, null, null);
		this.configPath = config;
		this.root = 'admin';
	}
	_userDir(userRoot: string, what: string) {
		return pathUtil.resolve(pathUtil.join(userRoot + pathUtil.sep + what));
	}
	_getConfigPath(args: IArguments): string {
		const user = this._getUser(this._getRequest(args));
		let configPath = this.configPath;
		if (user) {
			configPath = this._userDir(user, 'settings.json');
		}
		return configPath;
	}
	@RpcMethod
	public get(section: string, path: string, query?: any): IObjectLiteral {
		let configPath = this._getConfigPath(arguments);
		let data = this.readConfig(configPath);
		let result: IObjectLiteral = {};
		result[section] = dotProp.get(data, this.root + path + section);
		return result;
	}
	@RpcMethod
	public set(section: string, path: string = '.', searchQuery: any = null, value: any, decodeValue: boolean = true) {
		let data = this.readConfig(this._getConfigPath(arguments));
		const dataAt = dotProp.get(data, this.root + path + section);
		dataAt && _.extend(_.find(dataAt, searchQuery), value);
		return data;
	}
	@RpcMethod
	public update(section: string, path: string = '.', searchQuery: any = null, value: any = null, decodeValue: boolean = true) {
		return this.writeConfig(
			this._getConfigPath(arguments), this.set(section, path, searchQuery, value, decodeValue));
	}
	public read(path?: string): IObjectLiteral {
		return this.readConfig(this._getConfigPath(arguments));
	}
	public write(path?: string, val?: Object): void {
		this.writeConfig(path, val);
	}
	//
	// ─── DECORATORS
	//
	public getRpcMethods(): string[] {
		throw new Error("Should be implemented by decorator");
	}
	methods() {
		const methods = this.getRpcMethods();
		return this.toMethods(methods);
	}
}
