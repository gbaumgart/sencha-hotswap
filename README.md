### Installation

#### Global 

	npm install -g --production sencha-hotswap

#### Local 

	npm install --save-dev --production sencha-hotswap

### Usage

#### Via extending your package.json : 

```json

"hotswap": {
    "watch": [
      "./classic/src"
    ]
  },
 "scripts": {
    "watch":"sencha-hotswap"
 }

```

Now run: 

	npm run watch

#### Via CLI: 

Run
 
	sencha-hotswap --watch='../miimetiq/architect/modules/dashboard/frontend/classic/src,../miimetiq/architect/modules/dashboard/frontend/build/development'


#### Extend your index.html for


```html
	<!-- Sockjs -->
	<script id="sockjs" type="text/javascript" src="./node_modules/sencha-hotswap-client/client/sockjs-0.3.min.js"></script>
	<!-- Hotswap -->
	<script id="hotswap" type="text/javascript" src="./node_modules/sencha-hotswap-client/client/hotswap-client.js"></script>
```

#### Register your class for hotswap: 

```js

	Ext.define('Dashboard.view.MiimetiqDashboardComponentViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.miimetiq-dashboard-component',
	constructor: function () {
		RegisterHotswap(this);
	},
	....

```

#### Proceed as usual:
 - Fire sencha `sencha app watch'
 - Run `npm run watch` or if globally installed : `sencha-hotswap' inside your frontend folder (needs proper package.json or --watch argument )
 - update/insert methods of your class
 - save your file, some moment later  sencha-hotwatch will notify and update your client application in the browser

 
