var Ext = window['Ext'];
var SockJS = window['SockJS'];
/**
 * @private
 * @param key
 * @returns {*}
 */
function getKey(key) {
    var intKey = parseInt(key, 10);
    if (intKey.toString() === key) {
        return intKey;
    }
    return key;
}
/**
 * @private
 * @param type
 * @returns {*}
 */
/*
function toString(type) {
    return Object.prototype.toString.call(type);
}
*/
/**
     * internal set value at path in object
     * @private
     * @param obj
     * @param path
     * @param value
     * @param doNotReplace
     * @returns {*}
     */
function set(obj, path, value, doNotReplace) {
    if (Ext.isNumber(path)) {
        path = [path];
    }
    if (Ext.isEmpty(path)) {
        return obj;
    }
    if (Ext.isString(path)) {
        return set(obj, path.split('.').map(getKey), value, doNotReplace);
    }
    var currentPath = path[0];
    if (path.length === 1) {
        var oldVal = obj[currentPath];
        if (oldVal === void 0 || !doNotReplace) {
            obj[currentPath] = value;
        }
        return oldVal;
    }
    if (obj[currentPath] === void 0) {
        //check if we assume an array
        if (Ext.isNumber(path[1])) {
            obj[currentPath] = [];
        }
        else {
            obj[currentPath] = {};
        }
    }
    return set(obj[currentPath], path.slice(1), value, doNotReplace);
}
/**
 * deletes an property by a path
 * @param obj
 * @param path
 * @returns {*}
 */
function del(obj, path) {
    if (Ext.isNumber(path)) {
        path = [path];
    }
    if (Ext.isEmpty(obj)) {
        return void 0;
    }
    if (Ext.isEmpty(path)) {
        return obj;
    }
    if (Ext.isString(path)) {
        return del(obj, path.split('.'));
    }
    var currentPath = getKey(path[0]);
    var oldVal = obj[currentPath];
    if (path.length === 1) {
        if (oldVal !== void 0) {
            if (Ext.isArray(obj)) {
                obj.splice(currentPath, 1);
            }
            else {
                delete obj[currentPath];
            }
        }
    }
    else {
        if (obj[currentPath] !== void 0) {
            return del(obj[currentPath], path.slice(1));
        }
    }
    return obj;
}
/*
 * Private helper class
 * @private
 * @type {{}}
 */
var objectPath = {};
objectPath.has = function (obj, path) {
    if (Ext.isEmpty(obj)) {
        return false;
    }
    if (Ext.isNumber(path)) {
        path = [path];
    }
    else if (Ext.isString(path)) {
        path = path.split('.');
    }
    if (Ext.isEmpty(path) || path.length === 0) {
        return false;
    }
    for (var i = 0; i < path.length; i++) {
        var j = path[i];
        if ((Ext.isObject(obj) || Ext.isArray(obj)) && Object.prototype.hasOwnProperty.call(obj, j)) {
            obj = obj[j];
        }
        else {
            return false;
        }
    }
    return true;
};
/**
 * Define private public 'ensure exists'
 * @param obj
 * @param path
 * @param value
 * @returns {*}
 */
objectPath.ensureExists = function (obj, path, value) {
    return set(obj, path, value, true);
};
/**
 * Define private public 'set'
 * @param obj
 * @param path
 * @param value
 * @param doNotReplace
 * @returns {*}
 */
objectPath.set = function (obj, path, value, doNotReplace) {
    return set(obj, path, value, doNotReplace);
};
/**
 Define private public 'insert'
 * @param obj
 * @param path
 * @param value
 * @param at
 */
objectPath.insert = function (obj, path, value, at) {
    var arr = objectPath.get(obj, path);
    at = ~~at;
    if (!Ext.isArray(arr)) {
        arr = [];
        objectPath.set(obj, path, arr);
    }
    arr.splice(at, 0, value);
};
/**
 * Define private public 'empty'
 * @param obj
 * @param path
 * @returns {*}
 */
objectPath.empty = function (obj, path) {
    if (Ext.isEmpty(path)) {
        return obj;
    }
    if (Ext.isEmpty(obj)) {
        return void 0;
    }
    var value, i;
    if (!(value = objectPath.get(obj, path))) {
        return obj;
    }
    if (Ext.isString(value)) {
        return objectPath.set(obj, path, '');
    }
    else if (Ext.isBoolean(value)) {
        return objectPath.set(obj, path, false);
    }
    else if (Ext.isNumber(value)) {
        return objectPath.set(obj, path, 0);
    }
    else if (Ext.isArray(value)) {
        value.length = 0;
    }
    else if (Ext.isObject(value)) {
        for (i in value) {
            if (Object.prototype.hasOwnProperty.call(value, i)) {
                delete value[i];
            }
        }
    }
    else {
        return objectPath.set(obj, path, null);
    }
};
/**
 * Define private public 'push'
 * @param obj
 * @param path
 */
objectPath.push = function (obj, path /*, values */) {
    var arr = objectPath.get(obj, path);
    if (!Ext.isArray(arr)) {
        arr = [];
        objectPath.set(obj, path, arr);
    }
    arr.push.apply(arr, Array.prototype.slice.call(arguments, 2));
};
/**
 * Define private public 'coalesce'
 * @param obj
 * @param paths
 * @param defaultValue
 * @returns {*}
 */
objectPath.coalesce = function (obj, paths, defaultValue) {
    var value;
    for (var i = 0, len = paths.length; i < len; i++) {
        if ((value = objectPath.get(obj, paths[i])) !== void 0) {
            return value;
        }
    }
    return defaultValue;
};
/**
 * Define private public 'get'
 * @param obj
 * @param path
 * @param defaultValue
 * @returns {*}
 */
objectPath.get = function (obj, path, defaultValue) {
    if (Ext.isNumber(path)) {
        path = [path];
    }
    if (Ext.isEmpty(path)) {
        return obj;
    }
    if (Ext.isEmpty(obj)) {
        //Ext doesnt seem to work with html nodes
        if (obj && obj.innerHTML === null) {
            return defaultValue;
        }
    }
    if (Ext.isString(path)) {
        return objectPath.get(obj, path.split('.'), defaultValue);
    }
    var currentPath = getKey(path[0]);
    if (path.length === 1) {
        if (obj && obj[currentPath] === void 0) {
            return defaultValue;
        }
        if (obj) {
            return obj[currentPath];
        }
    }
    if (!obj) {
        return defaultValue;
    }
    return objectPath.get(obj[currentPath], path.slice(1), defaultValue);
};
/**
 * Define private public 'del'
 * @param obj
 * @param path
 * @returns {*}
 */
objectPath.del = function (obj, path) {
    return del(obj, path);
};
function GetExtClass(className) {
    return objectPath.get(window, className, null);
}
function GetExtClassPrototype(className) {
    var clazz = GetExtClass(className);
    if (clazz) {
        return clazz.prototype;
    }
}
function InspectExtClass(className) {
    var proto = GetExtClass(className);
    if (proto) {
        proto = proto.prototype;
        for (var p in proto) {
            if (typeof proto[p] !== 'function') {
                console.log(p, proto[p]);
            }
        }
    }
}
function InspectExtClassEx(proto) {
    if (proto) {
        proto = proto.prototype;
        for (var p in proto) {
            if (typeof proto[p] !== 'function') {
                console.log(p, proto[p]);
            }
        }
    }
}
var LAST_EX;
function MergeClass(oldClass, newClass) {
    for (var p in newClass) {
        if (typeof newClass[p] === 'function') {
            if (oldClass[p]) {
                oldClass[p] = newClass[p];
            }
            else {
                console.error('cant update ' + p);
            }
        }
    }
}
function UpdateExtJSClass(oldClass, newClass, clazz) {
    Ext.GlobalEvents.doFireEvent('testx', [{
            clazz: clazz,
            module: newClass,
            update: MergeClass
        }]);
}
function RegisterHotswap(who) {
    var clazz = who.$className;
    var self = who;
    Ext.GlobalEvents.on('testx', function (msg) {
        if (msg.clazz === clazz) {
            console.log('updated ' + clazz);
            msg.update(self, msg.module);
        }
    });
}
var InFileChanged = function (clazz, content) {
    var module = GetExtClassPrototype(clazz);
    if (module) {
        console.log('Ext.JS Hotswap: Class changed : ' + clazz);
        Ext.undefine(clazz);
        var newClass = eval(content);
        //InspectExtClassEx(newClass);
        LAST_EX = newClass;
        UpdateExtJSClass(module, newClass.prototype, clazz);
    }
    else {
        console.error('Ext.JS Hotswap Error: cant find class ' + clazz);
    }
};
function InitHotSwap() {
    var protocol = [
        'websocket',
        'xdr-streaming',
        'xhr-streaming',
        'iframe-eventsource',
        'iframe-htmlfile',
        'xdr-polling',
        'xhr-polling',
        'iframe-xhr-polling',
        'jsonp-polling'
    ];
    var options = {
        debug: 1,
        devel: true,
        noCredentials: true,
        nullOrigin: true,
        transports: null
    };
    options.transports = protocol;
    var host = "http://0.0.0.0";
    var port = 19999;
    var sock = new SockJS(host + ":" + port, null, options);
    var thiz = this;
    var debug = false;
    var reconnect = true;
    var ready = function () {
        console.info('Connected to Ext.JS Hotswap server');
    };
    sock.onopen = function () {
        ready();
    };
    sock.onmessage = function (msg) {
        var msg = JSON.parse(msg.data);
        var type = msg.event;
        var data = msg.data;
        debug && console.info('Ext.JS Hotswap server message : ' + type, data);
        if (type === 'FileChanged') {
            console.info('File changed : ' + data.class + ' @ ' + data.path);
            InFileChanged(data.class, data.content);
        }
    };
    sock.onerror = function () {
        debug && console.error('on sockjs error', arguments);
    };
    sock.onclose = function (e) {
        if (reconnect) {
            debug && console.log('closed ' + host + ' try re-connect');
            setTimeout(function () {
                InitHotSwap();
            }, 2000);
        }
        else {
            debug && console.log('closed ' + host, arguments);
        }
    };
}
setTimeout(function () {
    Ext.onReady(function () {
        InitHotSwap();
    });
}, 2000);
//# sourceMappingURL=hotswap-client.js.map