"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./applications/sencha-hotswap/index");
const pathUtil = require("path");
const fs_1 = require("fs");
const yargs_parser = require("yargs-parser");
const arrays_1 = require("@xblox/core/arrays");
const util_1 = require("util");
//just to include it in the build
const app = "sencha-hotswap";
process.on('unhandledRejection', (reason) => {
    console.error('Unhandled rejection, reason: ', reason);
});
const _argv = yargs_parser(process.argv.slice(1));
let isCli = _argv['_'][0].indexOf('sencha-hotswap') !== -1;
if (_argv.dev) {
    isCli = false;
}
exports.normalizePathString = (str) => {
    return arrays_1.coalesce(str.split(',').map((path) => {
        try {
            if (!fs_1.existsSync(pathUtil.resolve(path))) {
                console.error('Cant resolve path ' + path);
                return null;
            }
        }
        catch (e) { }
        return pathUtil.resolve(path);
    }));
};
if (!isCli) {
    const argv = yargs_parser(process.argv.slice(2));
    if (!argv.watch && !isCli) {
        console.error('You need to specicfy paths to be watched with --watch=...');
        process.exit(1);
    }
    const options = {
        paths: exports.normalizePathString(argv.watch),
        port: argv.port || 19999
    };
    if (!options.paths.length) {
        console.error('Have no paths to watch, abort');
        process.exit(1);
    }
    console.log('Watching paths : ', util_1.inspect(options.paths));
    const application = new index_1.Watcher(options);
    application.run().then((deviceServerContext) => { });
}
function createWithCLI(options) {
    console.log('Watching paths : ', util_1.inspect(options.paths));
    const application = new index_1.Watcher(options);
    application.run().then((deviceServerContext) => { });
}
exports.createWithCLI = createWithCLI;
//# sourceMappingURL=index.js.map