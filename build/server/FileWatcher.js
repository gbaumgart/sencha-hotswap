"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const file_1 = require("../io/file");
const chokidar = require('chokidar');
var FileChangeType;
(function (FileChangeType) {
    FileChangeType[FileChangeType["ADDED"] = 'added'] = "ADDED";
    FileChangeType[FileChangeType["CHANGED"] = 'changed'] = "CHANGED";
    FileChangeType[FileChangeType["DELETED"] = 'deleted'] = "DELETED";
    FileChangeType[FileChangeType["INTERNAL"] = 'internal'] = "INTERNAL";
})(FileChangeType = exports.FileChangeType || (exports.FileChangeType = {}));
class FileWatcher {
    constructor(paths, callback) {
        this.init(paths, callback);
    }
    init(paths, callback) {
        const watcher = chokidar.watch(paths, {
            ignored: (path) => false,
            persistent: true,
            ignoreInitial: true,
            awaitWriteFinish: true
        });
        watcher.on('addDir', (filePath) => {
            watcher.add(filePath);
            callback(filePath, FileChangeType.INTERNAL);
        });
        watcher.on('add', (filePath) => callback(filePath, FileChangeType.ADDED));
        watcher.on('unlinkDir', (filePath) => {
            watcher.unwatch(filePath);
            callback(filePath, FileChangeType.DELETED);
        });
        watcher.on('change', (filePath) => callback(filePath, FileChangeType.CHANGED, file_1.read(filePath)));
    }
}
exports.FileWatcher = FileWatcher;
//# sourceMappingURL=FileWatcher.js.map