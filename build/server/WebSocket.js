"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http = require("http");
const sockjs = require("sockjs");
const primitives_1 = require("@xblox/core/primitives");
const json_1 = require("../io/json");
class WebSocket {
    _handler() { }
    constructor(options) {
        this.options = options;
        this.clients = [];
        const socketOptions = options;
        const socket = sockjs.createServer(socketOptions);
        const socket_server = http.createServer(this._handler);
        const port = socketOptions.port;
        const host = socketOptions.host;
        socket.installHandlers(socket_server);
        socket_server.listen(port, host);
        this.handleSocketEmits(socket);
        this.socket = socket;
        this.socketServer = socket_server;
    }
    broadCastMessage(eventName, data) {
        let length = this.clients.length;
        while (length--) {
            const client = this.clients[length];
            if (client !== undefined) {
                const dataOut = {
                    event: eventName,
                    data: primitives_1.isString(data) ? json_1.deserialize(data) : data
                };
                if (client.readyState === 3) {
                    client.close();
                    this.clients.splice(length, 1);
                    continue;
                }
                this.clients[length].write(json_1.serialize(dataOut));
            }
        }
    }
    handleSocketEmits(socket) {
        const self = this;
        socket.on('connection', (conn) => {
            this.clients.push(conn);
            conn.on('close', function () {
                self.clients.splice(self.clients.indexOf(conn), 1);
                console.log('closed', self.clients.length);
            });
        });
    }
}
exports.WebSocket = WebSocket;
//# sourceMappingURL=WebSocket.js.map