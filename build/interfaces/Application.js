"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Application {
}
exports.Application = Application;
var EPersistence;
(function (EPersistence) {
    EPersistence[EPersistence["MEMORY"] = 'memory'] = "MEMORY";
    EPersistence[EPersistence["MONGO"] = 'mongo'] = "MONGO";
})(EPersistence = exports.EPersistence || (exports.EPersistence = {}));
//# sourceMappingURL=Application.js.map