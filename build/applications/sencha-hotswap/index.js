"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const WebSocket_1 = require("../../server/WebSocket");
const FileWatcher_1 = require("../../server/FileWatcher");
const esprima = require('esprima');
const esquery = require('esquery');
const DEBUG = false;
const SCRIPTS = /\.(js)$/i;
const STYLES = /\.(css)$/i;
const EXTEND = 'CallExpression > ObjectExpression > Property[key.name="extend"] [value]';
class Watcher {
    constructor(options) {
        this.options = options;
    }
    onChanged(path, type, content) {
        switch (type) {
            case FileWatcher_1.FileChangeType.CHANGED: {
                if (path.match(STYLES)) {
                    DEBUG && console.log('changed ' + path + ' Type : ' + type);
                }
                if (path.match(SCRIPTS)) {
                    try {
                        const ast = esprima.parse(content);
                        const clazz = ast.body[0]['expression']['arguments'][0].value;
                        const extend = esquery.query(ast, EXTEND);
                        this.socket.broadCastMessage('FileChanged', {
                            path: path,
                            class: clazz,
                            extent: extend,
                            content: content
                        });
                        console.log('changed class ' + clazz + ' @ ' + path);
                    }
                    catch (e) {
                        console.error('Error parsing script ' + path);
                    }
                }
            }
        }
    }
    run() {
        return new Promise((resolve, reject) => {
            this.socket = new WebSocket_1.WebSocket({
                "port": this.options.port,
                "host": "0.0.0.0",
                "debug": 0
            });
            this.watcher = new FileWatcher_1.FileWatcher(this.options.paths, (path, type, content) => {
                this.onChanged(path, type, content);
            });
        });
    }
}
exports.Watcher = Watcher;
//# sourceMappingURL=index.js.map